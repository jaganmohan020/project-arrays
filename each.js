

function each(elements,cb){

    let array = [];

    if(Array.isArray(elements) && elements.length >0 ){

        for(let index = 0; index < elements.length; ++index){
            cb ( elements[index] );         
        } 

    } else{
        return [];
    }
}



module.exports = each