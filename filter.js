function filter (elements,cb){
    let filtered = [];

    if(Array.isArray(elements) && elements.length  > 0 ){
    
    for(let element in elements){
        let index = parseInt(element)

        if(cb(elements[index],index,elements) === true) { 

            filtered.push(elements[index])
            
        }
        
    }
   
        return filtered;    
    } else {
        return [];
    }


}

module.exports = filter;