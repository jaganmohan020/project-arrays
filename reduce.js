function reduce(elements, cb, startingValue) {
    if (Array.isArray(elements) && elements.length > 0){
        if (startingValue === undefined){
            startingValue = elements[0]

            for(let index =1; index < elements.length; index++){
                startingValue=cb(startingValue,elements[index],index,elements);
            }

            return startingValue

        } else{

            for(let index = 0; index < elements.length; index++){
                startingValue = cb(startingValue,elements[index],index,elements)
            }
            return startingValue;
        }
    }
    else{
        return [];
    }
}

module.exports=reduce;
