
function find (elements,cb) {
    let value = 0;
    if(Array.isArray(elements) && elements.length >0 ){
        for (let index = 0; index < elements.length; index++){
        
            if(cb (elements[index])){
                value = elements[index];
                break;
        }

        }

        return value;

    } else {
        return [];
    }
}

module.exports = find;