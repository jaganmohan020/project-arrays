function flatten(array, depth =1) {
    if (!Array.isArray(array) || array.length == 0) {
      return [];
    }
    let ifNested;
    let count = 0;
    do {
      let changedArray = [];
      ifNested = false;
      for (let element of array) {
        if (Array.isArray(element)) {
          changedArray=changedArray.concat(element);
          ifNested = true;
        } else if (element === undefined || element === null) {
          continue;
        } else {
          changedArray.push(element);
        }
      }
      count++;
      array = changedArray;
      if (count === depth || !ifNested) {
        break;
      } else {
        continue;
      }
    } while (ifNested);
    return array;
  }
  
  module.exports = flatten;
